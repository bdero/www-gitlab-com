# Update the features

## Update the features page (under `/features`)

The feature page grabs its content automatically from the file
[`/data/features.yml`](/data/features.yml).

The minimal info required to add a new feature is:

```yaml
  - title: "Distinct feature name"
    description: "Feature description"
    screenshot_url: "images/feature_page/screenshots/09-gitlab-ci.png"
    link_description: "Learn more about feature name"
    link: link to docs or feature page
    feature_page: true
    gitlab_com: true
    gitlab_ce: true
    gitlab_ees: true
    gitlab_eep: true
```

## Create or update the comparison pages (under `/comparison`)

The [comparison page][comp] grabs its content automatically from
`data/features.yml`.

There are 3 files in total which you need to create or edit:

- `data/features.yml`: Update for new comparisons. Comparisons are automatically generated from the contents of this file.
- `source/comparison/gitlab-vs-competitor.html.haml`: Create for new comparisons.
  Every comparison page has its own html file. **Use dashes**
- `source/includes/comparison_table.html.haml`: Edit for new or existing
  comparisons.

---

**Step 1**

Add a new yaml file named after the comparison using underscores:
`data/comparisons/gitlab_vs_competitor.yml`. You can start by copying an
existing one and then changing its content. Yaml files are sensitive to
indentation, so be extra careful. Make sure that you follow the structure below:

```yaml
title: "Competitor vs GitLab"
pdf: gitlab-vs-competitor.pdf
competitor_one:
  name: 'GitLab'
  logo: '/images/comparison/gitlab-logo.svg'
competitor_two:
  name: 'Competitor'
  logo: '/images/comparison/competitor-logo.svg'
last_updated: 'May 5, 2017'
features:
  - title: "Briefly explain the feature"
    description: |
      Describe the differences in detail. This text can span in multiple
      lines without interfering with its structure. It will always appear
      as one paragraph.
    link_description: "Learn more about Feature Name."
    link: "link to GitLab's feature page documentation or blog post"
    competitor_one: true
    competitor_two: true
```

Notes:

- Although the file is named with GitLab being first, the title should have the
  competitor name first.
- The `pdf` name should be similar to the yaml file name, but with dashes. In
  short, it has to match the HTML page name in step 2. If you want to omit the
  PDF link altogether, set its value to `null`, like: `pdf: 'null'`.
- Remember to update the date every time you make a change.
- The competitor's logo (`competitor_two`) can be `svg` or `png`. Save it in
  `source/images/comparison/competitor-logo.svg`.
- In the features area, `competitor_one` is always GitLab, and `competitor_two`
  is the competitor we are compared against. Values for these two fields are
  `true|false|sortof`.

**Step 2**

Add a new haml file named after the comparison using dashes:
`source/comparison/gitlab-vs-competitor.html.haml`. You can start by copying an
existing one and then editing it. The only change you need to make is the
name of the yaml file you created above:

```yaml
---
layout: comparison_page
trial_bar: true
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
extra_js:
  - comparison.js
---

- data.features.gitlab_vs_competitor do |comparison_block|
  = partial "includes/comparison_table", locals: { comparison_block: comparison_block }
```

**Step 3**

As a last step, you need to add the new page in the dropdown menu. To do that,
open `source/includes/comparison_table.html.haml` and add an entry of your newly
added comparison page. Again, you can copy one of the entries you'll find inside
and just change its content. Haml files are sensitive to indentation, so be
extra careful:

```haml
%li
  = highlight_active_nav_link("Competitor vs. GitLab", "/comparison/gitlab-vs-competitor.html#dropdown")
```

**Step 4**

If you followed the above 3 steps, the new comparison page should be reachable
under `/comparison/gitlab-vs-competitor.html` and you should see it in the
dropdown menu. The last thing you need to do is create the PDF. Follow the
info in [creating comparison PDFs](#comparison-pdfs).

[comp]: https://about.gitlab.com/comparison/

## Update the gitlab-com features (under `/gitlab-com`)

