---
layout: markdown_page
title: "Finance"
---

## Communication
<a name="reach-finance"></a>

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/finance/issues/)- please use confidential issues for topics that should only be visible to team members at GitLab.

- [**Chat channel**](https://gitlab.slack.com/archives/finance)- please use the `#finance` chat channel in Slack for questions that don't seem appropriate for the issue tracker or internal email correspondence.

- **Accounts Payable**- inquiries which relate to vendor, merchant, and contractor invoices should be sent to our Accounts Payable mailbox - ap@gitlab.com. Of course, electronic copies of all invoices should be sent to this address upon receipt.

- **Accounts Receivable**- customer billing inquiries should be sent to our Accounts Receivable mailbox – ar@gitlab.com.

## Other pages

- [Signature authorization matrix](/handbook/finance/authorization-matrix)
- [Sales compensation plan](/handbook/finance/sales-comp-plan)
- [Analytics](/handbook/finance/analytics)

---

## On this page
{:.no_toc}

- TOC
{:toc}

---



## General Topics

### Regular compensation

For details on regular compensation, view the [People Ops page](/handbook/people-operations/#regular-compensation).

### Legal and Financial Information

This [document](https://docs.google.com/a/gitlab.com/document/d/1V7bUuCYZmK3FCZlR3TrZRZb8Li3BukRdNI-TGx7C-GE/edit?usp=sharing) contains various legal and financial information that is fundamental to the company, including but not limited to- corporate addresses, banking details, VAT registration numbers, etc. The accessibility of this information is restricted to GitLab team members. If you are looking for information that is not included in this document, send an inquiry to the Finance team.

### Corporate Metric Definitions
{: #definitions}

We track a wide range of metrics on our corporate dashboard.  Many definitions are self evident but some are not.

Annual Recurring Revenue (ARR) - Recurring revenue recognized in current month multiplied by 12.  (Source data from Zuora)

Average Sales Price (ASP) - IACV per won

Bookings Total Contract Value (TCV) - All bookings in period (including multiyear); bookings is equal to billings with standard payment terms. (Source data from Salesforce reconciled to Zuora)

Bookings Annual Contract Value (ACV) - Current Period subscription bookings which will result in revenue over next 12 months. (Source data from Salesforce reconciled to Zuora)

Bookings Incremental Annual Contract Value (IACV) <a name ="iacv"></a> - Value of new bookings from new and existing customers that will result in revenue of next 12 months. Also equals ACV less renewals. (Source data from Salesforce reconciled to Zuora)

Churn, Net - Current period revenue from customers present 12 months prior divided by revenue from 12 months prior. (Source data from excel; Zuora expected 2017-07-01)

Churn, Gross (Dollar weighted) - Remaining cohorts from Actual Subscription customers active as of 12 months ago multiplied by revenue from 12 months ago divided by actual Subscription customers as of date 12 months prior multiplied by revenue from 12 months ago. (Source data from excel; Zuora expected 2017-07-01)

Customer Acquisition Cost (CAC) - Total Sales & Marketing Expense/Number of New Customers Acquired (Source Zuora and Salesforce)

Rep Productivity - Current Month [Metric] /# of Reps on board for at least 90 days prior to start of period. (Source data bamboo HR)

Magic Number - IACV for trailing three months/Sales & Marketing Spend over trailing six months. (Source data Salesforce/Zuora and Netsuite)

MQL - Marketing Qualified Lead (Source data Salesforce)

SQL - Sales Qualified Lead (Source data Salesforce)

Cost per MQL - Marketing expense divided by # MQLs (Source data Salesforce and Netsuite)

Sales efficiency ratio - IACV / sales spend

Marketing efficiency ratio  - IACV / marketing spend

### Company Accounts
<a name="company-accounts"></a>

Login information for the following accounts can be found in the Secretarial vault
on 1Password:

- FedEx
- Amazon
- IND (Immigratie en Naturalisatie Dienst, in the Netherlands) company number

If you need this information but cannot find it in your shared vaults, check with PeopleOps to get access.

## Reimbursable Expenses

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee, etc.) and subsidiary assignment (Inc, LTD, BV). Check with PeopleOps if you are unsure about either of these. For information regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](https://about.gitlab.com/handbook/spending-company-money).

**Independent Contractors**– Contributors who are legally classified as independent contractors should include reimbursable expenses on the invoices they submit to the company through our Accounts Payable mailbox (ap@gitlab.com). These team members should also consider the terms and conditions of their respective contractor agreements, when submitting invoices to the company.

**Employees**– Contributors who are on GitLab’s payroll are legally classified as employees. These team members will submit expenses in the Expensify application. Expense reports are to be submitted once a month, at the least. If you were not assigned a user license as part of the onboarding process, you can request one by contacting PeopleOps. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here](https://docs.expensify.com/#search_query=getting%20started) and [here](https://docs.expensify.com/using-expensify-day-to-day/using-expensify-as-an-expense-submitter/report-actions-create-submit-and-close).

## Non-Reimbursable Expenses

Purchases of goods and services on behalf of the company can be facilitated by sending requests to our Accounts Payable mailbox (ap@gitlab.com). Note that this does **not** include travel expenses and other incidentals. These expenses should be self-funded then submitted for reimbursement within Expensify, or for independent contractors, included in invoices to the company (per the guidelines above). Once a request is made, Finance will reach out to coordinate payment setup. Most importantly, the team member making the purchase request is ultimately responsible for reviewing and approving the expenses, on a regular basis. All original invoices and payment receipts must be sent to AP (ap@gitlab.com).

## Company Credit Cards
<a name="company-cc"></a>

Team members carrying company cards in their name should submit the expenses within Expensify, on a monthly basis. These expense reports should tie to your monthly account statement which can be accessed in the American Express online portal. The statement cutoff is the 29th of each month. Of course, it is important to remember to attach all of the necessary receipts. If you have any questions on this policy, don't hesitate to reach out to the Finance team.

Although we have worked to reduce the number of outstanding company credit cards in an effort to centralize corporate purchasing, there are still certain situations where it may be more practical to issue additional corporate cards. Please contact the Finance team if you would like further information on this.  

If possible, team members carrying company cards should link their cards to Expensify. To do so, follow the below steps:

1. Register your card with [American Express](http://www.americanexpress.com/confirmcard).
1. Log into [Expensify](https://www.expensify.com/inbox) and click on your picture in the top right corner. Choose "Account Settings".
1. On the left panel, choose the option for "Credit Cards".
1. Choose "Import Card/Bank".
1. Choose "American Express US (Business)".
1. Provide your log-in credentials that you created when you registered with American Express.
1. Enter in your card details. It will take a few minutes for the card to import, so do not click away from this page.

## Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and summits.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

## Renting Cars in the United States and Canada

### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the USA or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that Gitlab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

### Physical Damage – Collision Damage Waiver

**Do not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

## Renting Cars- Countries other than the U.S.A. and Canada

### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the USA and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

### Physical Damage – Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the USA and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.

### Personal Use by Employee or Family Members of Business Auto Rentals

Coverage is **not** provided for personal use of automobiles or when family members are driving. Please evaluate whether your own personal automobile insurance provides an extension for this coverage. If it does not, or you are renting a vehicle outside the U.S.A. or Canada or taking a USA rented vehicle into Mexico, we recommend that you purchase the liability and physical damage coverage offered by the rental agency to protect your personal liability when not engaged in company business. GitLab will not pay for liability or damage to the rental vehicle resulting from personal use or use by non-employees.


## Finance Technology Stack

The finance and accounting tech stack includes the following applications:

- **NetSuite**- The company Enterprise Resource Planning (ERP) system. The application is cloud based and allows enhanced dimensional reporting as well as multi-currency/multi-entity reporting. This is where the General Ledger resides and all financial activity is ultimately recorded, which is critical to reporting the financial health of the company.

- **Zuora**- Customer billing is processed using Zuora, a cloud based subscription management platform. All invoicing, both recurring and non-recurring, is managed in Zuora.

- **Stripe**- Stripe is a software application that enables GitLab customers to make online payments. Finance uses this system to collect information pertaining to payments made online.

- **Expensify**- The company travel and expense management application. The platform provides employees a simple tool to create expense reports, which can then be managed from approval workflow through the final stage of reimbursement. Expensify is integrated with NetSuite, which further automates the processing of employee expenses by the Finance team.

- **eShares**- Stock option administration, capitalization table and equity management are all managed using the eShares software platform.

- **Salesforce**- Salesforce is the company CRM platform. This application primarily functions under Sales and Marketing, but inevitably, contains information that is also used by the Finance team.

## Month-End Close Checklist

This [link](https://docs.google.com/a/gitlab.com/spreadsheets/d/1SSUQpudxxpPgXIS97Ctuj-JRII0qhq0I3r19jmBKU7c/edit?usp=sharing) will take you the check list used by Finance to track the month-end close process. This is updated regularly to reflect our progress in closing the books for any given month.

## Processing Payroll
<a name="payroll"></a>

### GitLab Inc. Payroll Procedures
<a name="payroll-inc"></a>

1. You will receive a reminder from TriNet the week before payroll closes for each pay period.
1. If you are handling payroll changes- the email will let you know by what date you must submit those changes.
1. Enter the dates into your calendar with a reminder for the day before the deadline.
1. Log into TriNet Passport
1. My Company=>My Workplace=>Payroll Entry & Admin
1. You are now on the payroll dashboard
1. Select the current pay period from the calendar screen in the middle of the page.
1. Each employee will have a row with the information necessary to document hours (if hourly) and other earnings (this can be commission, reimbursement, etc. There is a drop down menu of options and codes from which to choose).
1. After you have updated each employee, select the save button.
1. Do not use the Submit button. It will make adding last minute changes difficult and TriNet will take your last save when processing payroll.
1. Make sure that you calculate the hours based on the pay days (count them) for each pay period.

All hourly time sheets are kept on the Google Drive and shared with Finance. Each employee will populate the time sheet before the end of the pay cycle.

#### Additional Begin/End Payment (for adding payment to payroll)

1. In HR Passport click Find
1. Select find by Name
1. Click on Add’l Begin/End Payment
1. Select Action Type
1. Select Begin & End date (End date is optional.  Note: The Begin date needs to be the start of a pay period,  it cannot be in the middle or end of a pay period)
1. Enter Goal Amount (if applicable)
1. Enter the Earn Type
1. Enter the amount per pay period
1. Enter pay frequency

Please see People Ops for more information on [making changes in TriNet](https://about.gitlab.com/handbook/people-operations/sop/#making-changes-in-trinet).

#### Timesheets for Hourly Employees

1. People Ops and Finance will share a private Google Sheet with you where you will log your hours for each day in the “hours” column.
1. There is a dropdown in the “pay type” column, with the default being Regular. There are also options for Overtime, Vacation, Sick, and Bereavement. Choose the appropriate pay type for your time.
1. If you work overtime or more hours than agreed upon in your contract, please obtain approval from your manager and forward to Finance before payroll cutoff.
1. Your timesheet is due one day prior to the submit payroll date, which is outlined for the calendar year on your timesheet.

### GitLab BV Pay Slip Distribution Process
<a name="payroll-bv"></a>

All GitLab BV employees receive their payslips within their personal portal of Savvy.
They can login and download their payslip to their computer if needed.

### UK, Belgium, & India Monthly Payroll Process

Payroll cut off for sending changes is usually the middle of the month (15th-17th), in addition, expenses for the UK should also be submitted at this time as they are paid via the payroll.The payroll provider will send a report for approval to the People Operations email address and copy in the Financial Controller. The Financial Controller will approve the payroll by sending a confirmation email back to the payroll provider. Once processed the payslips are sent electronically directly to the team members for them to access via a password protected system.

## Invoice template and where to send
<a name="invoices"></a>

NOTE: Vendor and contractor invoices are to be sent to ap@gitlab.com. An invoice
template can be found in Google Docs by the name of "Invoice Template".

### A note on VAT for transactions between GitLab BV and EU-based vendors

In many cases, VAT will not be payable on transactions between GitLab BV and EU-based
vendors/contractors, thanks to "Shifted VAT". To make use of this shifted VAT:

* The vendor/contractor writes the phrase "VAT shifted to recipient according to
article 44 and 196 of the European VAT Directive" on the invoice along with the
VAT-number of GitLab BV (NL853740343B01).
* On the vendor's VAT return the revenue from GitLab BV goes to the rubric "Revenue within the EU". It goes without saying that vendors are responsible for their own correct and timely filings.
* GitLab BV files the VAT on the VAT return, and is generally able to deduct this VAT all as part of the same return.


### Processing payment for invoices

1. Upon receipt of vendor invoices:
    * File a .pdf copy of the invoice to dropbox\For Approval.
    * Notify manager of new invoices to be approved by forwarding the email from the vendor.
    * Invoices are to be approved based on signed agreements, contract terms, and or purchase orders.
    * After review, manager to reply to email with “Approved”. An audit trail is required and this email will serve this purpose.
1. On approval, move the invoice from dropbox\For Approval to dropbox\Inbox
1. Post the invoice through accounting system. Before paying any vendor (for Inc. only), be sure there is a W-9 on file for them.
1. On a daily basis, generate an AP aging summary from the accounting system and identify invoices to be paid.
1. Initiate payment(s) through the bank (Comerica/Rabobank) and notify management that there is a pending payment.  Include a summary of invoices being paid.
1. Verify the payment has cleared the bank.
1. Upon verified payment of the invoice move the .pdf copy of the invoice from dropbox\Inbox to folder inbox\”vendor name”.
1. Post the payment through the accounting system.

### NetSuite

Invoices will arrive by email to ap@gitlab.com.

1. Forward email to Sytse, or the appropriate GitLab team member, for approval.
1. Create a PDF copy of the email containing the approval response.
1. File the invoice and approval in the "Invoices" folder in Google Drive.
1. Enter the invoice in NetSuite. Step by step instructions for this process are below.

#### Entering a Bill (invoice) in NetSuite

1. On the home page, click the “+” icon near the global search bar at the top of the screen and select “Bill."
1. Select the appropriate vendor record. If adding a new vendor, follow the bullets below before proceeding, otherwise skip to step 3.
    * Enter the company name, email address, applicable subsidiary, physical address, payment terms, primary currency, and Tax ID. (Note that the address field is located under the "Address" tab, while the Tax ID, primary currency, and payment terms fields are located under the "Financial" tab)
    * Enter the banking information in the "Comments" field then click “Save.”
    * Go to the "+" icon at the top of the vendor record and select "Bill" from the dropdown box.
1. Enter Bill date. The due date should auto-fill based on payment terms entered during vendor setup. If not, select the correct due date and update the vendor record after the bill has been entered and saved.
1. Enter Bill number.
1. Go to the "Expense and Items" tab below to enter the expense details.
1. Select appropriate GL-account under the "Account" dropdown box. (Be sure to check whether the invoice represents a prepaid expense, fixed asset, etc.)
1. Enter Bill amount.
1. Select tax code, if applicable.
1. Enter department. (This must be entered if the account you selected in step 6 is an expense account)
1. Add attachments: Go to the "Communication" tab and find the "Files" subtab.
1. Click "New File.” A new window will appear, allowing you to select the file you wish to attach.
1. In the new window, select the "Attachments Received" folder in the dropdown box, then click "Choose File" to attach both a copy of the vendor bill and email approval. (The supporting email approval must be attached along with a copy of the invoice)
1. Click "Save.”
1. In Google Drive, file invoice in the “Unpaid” folder.

## Commission Payment Process
<a name="commission"></a>

1. Each sales person will receive their own calculation template.
1. Salesperson is to complete their monthly template four days (payroll will send reminder) prior to first payroll of the month. Upon completion, salesperson will ping a manager for review and approval.
1. Approving manager will ping accounting upon approval.
1. Accounting will review and reconcile paid vs unpaid invoices.
1. Accounting will note in calculation template the amounts to be paid in commission.
1. Accounting will ping payroll that commission calculation is complete.
1. Payroll will enter commission into TriNet.

## Processing Expensify Reports (Finance Team)

1. In Expensify, go to the “Reports” tab.
1. Filter the listed reports and narrow the results to reports that are marked as “Reimbursed.”
1. Click the checkbox next to each report that you wish to export to NetSuite.
1. Once the reports have been selected, go to the “Export to” dropdown menu near the top of the page and click “NetSuite.”
1. A box will appear showing the real-time status of the export. The process is complete once this box confirms a successful export. If you receive an error message, see the information below and follow the remaining steps.
    * Unsuccessful exports typically result from an error related to NetSuite records. Each Expensify user must have a corresponding NetSuite Employee record and Vendor record.
    * Check NetSuite to verify if the employee in question has both a Vendor and Employee record.
    * If the records exist, add the employee’s email address to both records. In order for the export to work, this must be the same email address that is assigned to the employee within Expensify.
    * If the records do not exist, create both an Employee and Vendor record in NetSuite, complete with email addresses. Expensify uses the employee email address when mapping its records to that of NetSuite. The export will not work without proper alignment of email addresses in the NetSuite and Expensify records.

## Accounts Receivable
<a name="ar"></a>

### Invoicing

1. Log in to Zuora and Salesforce.
1. In Salesforce, click the "+" icon and select "Z-Quotes".
1. Subscriptions to be invoiced are sorted by date. Choose a subscription from today's date and proceed.
1. Still in Salesforce, check the customer account for a current PO number under the notes and attachments.
1. Then in Zuora, search for the corresponding customer subscription and open the record.
1. Check to see if the PO from the file in Salesforce matches that of Zuora. If not, update the record in Zuora with the latest PO number.
1. Verify that the "Bill to" and "Sold to" contacts listed in the Zuora customer record match the related record in Salesforce. If not, update Zuora.
1. Verify that the payment terms and method listed in Zuora match the related record in Salesforce. If not, update Zuora.
1. In Zuora, scroll down to transactions and click "Create Bill Run."
1. It is important to find the subscription start date in Salesforce and enter it in the "Target Date for this bill run" field in Zuora. The invoice date should be today's date.
1. Once these dates agree, click "Create Bill Run."
1. On the following screen, select the bill run that was just created (the bill run files begin with BR- followed by a set of numbers).
1. Scroll down and click on the invoice that was just generated. The PDF file will be at the bottom of the page.
1. Open the PDF file and review every field of the invoice for accuracy.
1. Once the review is complete, close the invoice and click “Post Invoice.”
1. Proceed to bill the remaining subscriptions.

At this point the invoicing process is complete. Now, continue on to the Cash Receipt posting process for those invoices that were paid by credit card.

## Cash Receipt
<a name="cash-receipt"></a>

### Credit card customer

Follow this procedure if the customer paid by credit card.
You may recall from the invoicing process that there was still a balance due when saving the invoice.  The following steps will record the payment and remove the balance due.

1. Login to Stripe dashboard and click on Payments under Transactions (left hand side). You will see a listing of the latest Stripe transactions listed by amount, Recurly transaction, name, date, and time. There is also an option to filter the report by clicking on XXX at the top left. Click on XXX to export to excel. This will give you a workbook area and also a breakdown of the fees which we will work on later.
1. In NetSuite, click on the "Transactions" tab on the left.
    * Click on the orange "OPEN INVOICES " tab. This will bring up all open invoices listed by date, invoice #, customer, etc.
1. Match invoice #s  between the Stripe dashboard and NetSuite. If you click on a transaction in the Stripe dashboard, it will take you to a screen that shows more detail, including the invoice # being paid. You can work your way from the bottom up.
1. In NetSuite, click "Receive Payment" on the matched payment and invoice.
1.  Receiving the payment
    * Enter the payment date, which is the payment date from Stripe dashboard.
    * Payment method = Credit Card.
    * Reference no. = "Recurly Transaction ID:" found under Metadata in Stripe dashboard.
    * Deposit to = Stripe.
    * NetSuite will auto-fill the payment amount with the entire balance due. No need to change this unless the payment amount from Stripe is different.
    * Click on "Save and Close".
    * Repeat the above for all the remaining invoices that were paid by credit card.

1. Post a journal entry to record Stripe Fees.
    * In NetSuite, click on the "+" sign. Under "Other", select "Journal Entry".
    * It is okay to leave the journal date as long as it is within the month the fees were incurred. If not, change it to the last day of the month.
    * NetSuite will auto fill the journal number. Do not change.
    * Account #1 Entry
      * Fill the "Account #1" entry with "Credit Card Transaction fees".
      * Fill the "Debits" entry with the value from the Stripe report that was exported. The value will be the sum of "Column I" in the Stripe report, which is the fee amount. Be sure to only sum the rows which you just posted payments for.
      * Leave the "Credits" entry empty.
      * Fill the "Description" entry with "To record credit card transaction fees for period (enter the date range for the transactions posted)".
      * Leave the "Name" entry empty.
      * Fill the "Class" entry with "Sales".
    * Account #2 Entry
      * Fill the "Account #2" entry with "Stripe".
      * Leave the "Debits" entry empty.
      * The "Credits" entry will autofill. This should be the same amount as the "Debits" entry for Account #1.
      * The "Description" entry will autofill. This should be the same as the "Description" entry for Account #1.
      * Leave the "Name" entry blank.
      * Leave the "Class" entry blank.
      * Click "Save".

This transaction transfers the payment obligation from the customer to Stripe.  The payment obligation from Stripe is removed when Stripe transfers  the funds to GitLab's bank account.

### Posting a payment from Stripe when a transfer is received from Stripe.

Post a journal entry:
1. Fill the "Journal Date" with the date that payment was received in the bank.
1. Fill the "Credit Account" with Stripe.
1. Fill the "Debit Account" with "Comerica Checking - GitLab Inc."
1. Leave "Name" blank.
1. Leave "Class" blank.
1. Fill the "Description" with "To record Stripe transfer (date of transfer)".
1. Click "Save".


### Posting a payment from a “bank customer”

In Netsuite:
1. Click on the “+” sign.
1. Click on “Receive Payment” under Customers.
1. Fill the "Payment Date" with the date payment was received.
1. Fill the "Payment Method" choose from the dropdown menu.
1. Fill the "Reference No." with the check # or bank reference # from incoming wire.
1. Fill the "Deposit to" with "Comerica Checking".
1. Fill the "Amount Received" with the amount received from the incoming wire.

## Asset Tracking

Items paid for by the company are property of the company.

Assets with purchasing value in excess of $1000 USD are tracked in BambooHR, for assets of lower value we rely on the honor system. These assets come to People Ops attention by one of two ways: 1. People Ops makes the purchase on behalf of the team member, or 2. the Finance team notices the line items on expense reports / invoices and passes this along to People Ops.

The information is then entered into BambooHR (to track who has which piece of equipment) by People Ops, and it is included in the Fixed Asset Schedule by Finance (to track asset value and depreciation).

1. Go to the team member in BambooHR
1. Click on the Assets Tab
1. Click Update Assets
1. Enter Asset Category, Asset Description, Serial Number, Asset Cost, and Date Loaned
1. This process is repeated for each asset purchased

If a team member would like to purchase an asset from the company (i.e. a laptop), please email People Operations to obtain the amount to be paid. This is derived from original cost less accumulated depreciation.
