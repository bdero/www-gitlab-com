---
layout: markdown_page
title: "Performance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Related Pages
{:.no_toc}

- [GitLab.com (infra) architecture](handbook/infrastructure/production-architecture/)
- [Monitoring GitLab.com](handbook/infrastructure/monitoring/)
- [Application Architecture documentation](https://docs.gitlab.com/ce/development/architecture.html)
- [GitLab.com Settings](https://about.gitlab.com/gitlab-com/settings/)
- [GitLab performance monitoring documentation](https://docs.gitlab.com/ce/administration/monitoring/performance/introduction.html)

## Flow of information in various scenarios, and its performance

Issue that spawned this page: <https://gitlab.com/gitlab-com/infrastructure/issues/1878>

All items that start with the <i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>
symbol represent a step in the flow that we _measure_ . Wherever possible, the
tachometer icon links to the relevant dashboard in our [monitoring](handbook/infrastructure/monitoring/).

### Flow of web request

Considering the scenarios of a user opening their browser, and surfing to their dashboard
by typing `gitlab.com/dashboard`, here is what happens:

1. User enters gitlab.com/dashboard in their browser and hits enter
1. Browser looks up IP address in DNS server
   - DNS request goes out and comes
back (typically ~10-20 ms, [can use link to data]; often times it is already cached so
  then it would be faster).
   - We use Route53 for DNS, and will start using DynDNS soon as well.
   - For more details on the steps from browser to application, enjoy reading
    <https://github.com/alex/what-happens-when>
   - not measured
1. From browser to load balancers
   - Now that the browser knows where to find the IP address, browser sends the web
request (for gitlab.com/dashboard) to Azure; Azure determines where to route the
packet (request), and sends the request to our Frontend Load Balancer(s) (also
  referred to as HAProxy).
   - not measured
1. HAProxy (load balancer) does SSL negotiation with the browser (takes time)
   - not measured
   - [todo, follow up on "there are some Web settings that may help: <https://linux-audit.com/optimize-ssl-tls-for-maximum-security-and-speed/>"]
1. HAProxy forwards to NGINX in one of our front end workers
   - In this case, since we are tracking a web request, it would be the nginx box in the
     "Web" box in the production-architecture diagram; but alternatively the request can come in via API or a git command
     from the command line, hence the API, and git "boxes")
    - Since all of our servers are in ONE Azure VNET, the overhead of SSL
      handshake and teardown between HAProxy and NGINX should be close to negligible.
    - not measured
1. NGINX gathers all network packets related to the request ("request buffering")
   - the request may be split into multiple packets by the intervening network,
   for more on that, read up on [MTUs](https://en.wikipedia.org/wiki/Maximum_transmission_unit).
   - In other flows, this won't be true. Specifically, request buffering is
   [switched off for LFS](https://gitlab.com/gitlab-org/gitlab-workhorse/issues/130)
   - not measured, and not in our control.
1. NGINX forwards full request to workhorse (in one combined request)
   - not measured
1. Workhorse splits the request into parts to forward to
   - [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=13&fullscreen&orgId=1)Unicorn (time spent waiting for Unicorn to pick up a request = `HTTP queue
      time`).
   - [not in this scenario, but not measured in any case] Gitaly
   - [not in this scenario, but not measured in any case] NFS (git clone through HTTP)
   - [not in this scenario, but not measured in any case] Redis (long polling)
1. Unicorn (often just called "Rails", or "application server"), translates the
request into a Rails controller request; in this case `RootController#index`. RailsController requests are sent to:
  - [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=9&fullscreen&orgId=1) PostgreSQL (`SQL timings`),
  - [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=14&fullscreen&orgId=1) NFS (`git timings`),
  - [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=13&fullscreen&orgId=1) Redis (`cache timings`).
  - In this `gitlab.com/dashboard` example, the controller addresses all three [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&var-action=RootController%23index&var-database=Production).
  Typically 20 ms in cache, git timings in the 100's of ms (peaky), sql timings
  (mean in 10's of ms, peaks to 5 s).
  - There are usually _multiple_ SQL calls (or file, or cache, etc.) calls for a given
  controller request. These add to the overall timing, especially since they are
  sequential. For example, in
  this scenario, there are [29 SQL calls (search for `Load`)](http://profiler.gitlap.com/20170524/901687e2-9fa1-4256-8414-c4835dc31dbc.txt.gz)
  when this _particular user_ hits `gitlab.com/dashboard/issues`. The number of SQL calls
  will depend on how many projects the person has, how much may already be in cache, etc.
   - There's generally no multi-tasking within a single Rails request. In a
   number of places we multi-task by serving a HTML page that uses AJAX to
   fill in some data, for example on `gitlab.com/username` the contribution
   calendar and the "most recent activity" sections are loaded in parallel.
   - In the Rails stack, middleware typically adds to the number of round trips
   to Redis, NFS, and PostgreSQL, per controller call, in addition to the
   timings of Rails controllers.  Middleware is used for {session state, user
  identity, endpoint authorization, rate limiting, logging, etc} while the
  controllers typically have at least one round trip for each of {retrieve
  settings, cache check, build model views, cache store, etc.}. Each such
  roundtrip _estimated_ to take < 10 ms.
1. Unicorn receives the information from the database, NFS, and cache
   - no data on the round trip time for asking / receiving the data.
1. [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=8&fullscreen&orgId=1)  Unicorn constructs the relevant html blob (view) to be served back to the user.
   - In our gitlab.com/dashboard example, view timings p99 in multiple seconds
   with mean < 1s. See the `View Timings`.
   - A particular view in Rails will often be constructed from multiple partial
    views. These will be used from a template file, specified by the controller
    action, that is, itself, generally included within a layout template.
    Partials can include other partials. This is done for good code
    organization and reuse. As an example, when the _particular user_  from the
    example above loads `gitlab.com/dashboard/issues`, there are [56 nested / partial views rendered (search for `View::`)](http://profiler.gitlap.com/20170524/901687e2-9fa1-4256-8414-c4835dc31dbc.html.gz)
   - GitLab renders a lot of the views in the backend (i.e. in Unicorn) vs.
   frontend. To see the split, use your browser's "inspect" tool and look at
   TTFB (time to first byte, this is the browser waiting to hear anything back,
     which is due to work happening in the backend) and compare it to the
     download time.
   - Some of these blobs are expensive to compute, and are sometimes hard-coded
   to be sent from Unicorn to Redis (i.e. to cache) once rendered.
1. [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1) Unicorn sends html blob back to workhorse
  - The round trip time it takes for a request to _start_  in Unicorn and _leave_ Unicorn
  is what we call `Transaction Timings`.
1. Workhorse sends html blob to NGINX
  - not measured
1. NGINX sends html blob to HAProxy
  - not measured
1. HAProxy send blob to Azure load balancer
  - not measured
1. Azure load balancer sends blob to browser
1. Browser renders page.
  - not measured
  - The rendering refers to the html blob. However, the browser also needs to
  load JS, CSS, images, and webfonts before the user can interact with it. As
  the page is streamed to the browser, the browser will be incrementally parsing
  it, looking for additional resources that it can start fetching. If these
  resources are on a different hostname, the browser will need to perform
  further DNS lookups (see step 2). For more, see the [related
  issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/33501).

### Flow of git commit push

First read [Flow of web request](#Flow-of-web-request) above, then pick up the
thread here.

After pushing to a repository, e.g. from the _web UI_:

1. In a web browser, make an edit to a repo file, type a commit message, and
 hit "Commit"
1. NGINX receives the git commit and passes it to Workhorse
1. Workhorse launches a `git-receive-pack` process (on the workhorse machine)
to save the new commit to NFS
1. On the workhorse machine, `git-receive-pack` fires a [git hook](/glossary/#git-hook)
to trigger `GitLab Shell`.
   - GitLab Shell accepts Git payloads pushed over SSH and acts upon them (e.g.
     by checking if you're authorized to perform the push, scheduling the data
     for processing, etc).
   - In this case, GitLab Shell provides the `post-receive` hook, and
the `git-receive-pack` process passes along details of what was pushed to the
repo to the `post-receive` hook. More specifically, it passes a list of three items: old
revision, new revision, and ref (e.g. tag or branch) name.
1. Workhorse then passes the `post-receive` hook to Redis, which is the Sidekiq queue.
   - Workhorse informed that the push succeeded or failed (could have failed due to the repo not available, Redis being down, etc.)
1. Sidekiq picks up the job from Redis and removes the job from the queue
1. Sidekiq updates PostgreSQL
1. Unicorn can now query PostgreSQL.


## Availability and Performance Priority Labels
{: #performance-labels}

To clarify the priority of issues that relate to GitLab.com's availability and
performance consider adding an _Availability and Performance Priority Label_,
`~AP1` through `~AP3`. This is similar to what is in use in the Support and
Security teams, they use `~SP` and `~SL` labels respectively to indicate
priority.

Use the following as a guideline to determine which Availability and Performance
Priority label to use for bugs and feature proposals. Consider the _likelihood_
and _urgency_  of the "scenario" that could result from this issue (not) being
resolved.

- **Urgency:**  _Examples_
 - U1
    - Outage likely within a month.
    - Affects many team members and/or many GitLab.com users
 - U2
    - Outage likely within three months.
    - Affects some team members and/or a few GitLab.com users
 - U3
    - Outage can happen, but not likely in next three months.
    - Affects some team members but no GitLab.com users

- **Impact:** _Examples_
 - I1
    - Outage of >= 25 minutes.
    - Performance improvement (or avoiding degradation) of >= 100 ms expected.
 - I2
    - Outage of 5 - 25 minutes.
    - Performance improvement (or avoiding degradation) of 10-100 ms expected.
 - I3
    - Outage of 0 - 5 minutes.
    - Performance improvement (or avoiding degradation) of <= 10 ms expected.


| **Urgency \ Impact**          | **I1 - High** | **I2 - Medium**  | **I3 - Low**   |
|----------------------------|---------------|------------------|----------------|
| **U1 - High**              | `AP1`         | `AP1`            | `AP2`          |
| **U2 - Medium**            | `AP1`         | `AP2`            | `AP3`          |
| **U3 - Low**               | `AP2`         | `AP3`            | `AP3`          |


## Database Performance

Some general notes about parameters that affect database performance, at a very
crude level.

- From whitebox monitoring,
   - Of time spent on/by Rails controllers, this much is spent in the database: https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&panelId=5&fullscreen (for a specific Rails controller / page)
   - _Global_ SQL timings: https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=9&fullscreen&orgId=1&from=now-2d&to=now
- A single HTTP request will execute a single controller. A controller in turn
will usually only use one available database connection, though it may use 2 if
first a read was performed, followed by a write.
   - pgbouncer allows up to 150 concurrent PostgreSQL connections. If this limit
is reached it will block pgbouncer connections until a PostgreSQL
 connection becomes available.
    - PostgreSQL allows up to 300 connections (connected, whether they're active
    or not doesn't matter). Once this limit is reached new connections will be
    rejected, resulting in an error in the application.
    - When the number of processes > number of cores available on the database
    servers, the CPU constantly switches cores to run the requested processes;
    this contention for cores can lead to degraded performance.
- As long as the database CPU load < 100% (http://monitor.gitlab.net/dashboard/db/postgres-stats?refresh=5m&orgId=1&from=now%2Fw&to=now&panelId=13&fullscreen),
then in theory the database can handle more load without adding latency. In
practice database specialists like to keep CPU load below 50%.
    - As an example of how load is determined by underlying application design:
     DB CPU percent used to be lower (20%, prior to 9.2, then up to 50-75% [when
       9.2 RC1 went live](https://gitlab.com/gitlab-org/gitlab-ce/issues/32536),
       then back down to 20% by the time 9.2 was released.
- pgbouncer
   - What it does: pgbouncer maps _N_ incoming connections to _M_ PostreSQL
   connections, with _N_ >= _M_ (_N_ < _M_ would make no sense). For example,
   you can map 1024 incoming connections to 10 PostgreSQL connections. This is mostly influenced by the number of
concurrent queries you want to be able to handle. For example, for GitLab.com
our primary rarely goes above 100 (usually it sits around 20-30), while
secondaries rarely go above 20-30 concurrent queries. The more secondaries you
add, the more you can spread load and thus require fewer connections (at the
  cost of having more servers).
   - Analogy: pgbouncer is a bartender serving drinks to many customers. Instead
   of making the drinks himself she instructs 1 out of 20 “backend” bartenders
   to do so. While one of these bartenders is working on a drink the other 19
   (including the “main” one) are available for new orders. Once a drink is done
   one of the 20 “backend” bartenders gives it to the main bartender, which in
   turn gives it to the customer that requested the drink. In this analogy, the
  _N_ incoming connections are the patrons of the bar, and there are _M_ "backend"
   bartenders.
   - Pgbouncer frontend connections (= incoming ones) are very cheap, and you
   have have lots of these (e.g. thousands). Typically you want _N_ >= _A_ with
   _N_ being the pgbouncer connection limit, and _A_ being the number of
   connections needed for your application.
   - PostgreSQL connections are much more expensive resource wise, and ideally
   you have no more than the number of CPU cores available per server (e.g. 32).
   Depending on your load this may not always be sufficient, e.g. a primary in
   our setup will need to allow 100-150 connections at peak.
   - Pgbouncer can be configured to terminate PostgreSQL connections when idle
   for a certain time period, conserving resources.
