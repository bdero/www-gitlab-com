$(function() {
  var internalNavigationEvent = 'onpopstate' in window ? 'popstate' : 'hashchange';
  var scrollToDistro = function scrollToDistro(el) {
    $(window).scrollTop(el.offset().top - 230);
  };
  var showDistro = function showDistroy(el) {
    el.removeClass('hidden')
      .addClass('is-active')
      .prev()
      .addClass('is-active');

    setTimeout(function() {
      scrollToDistro(el);
    });
  };

  if (location.hash) {
    showDistro($(location.hash));
  }

  var $allDistros = $('.js-distro-content');
  $('.js-distro-tile').on('click', function(e) {
    var isOpen = this.parentNode.classList.contains('is-active');
    $allDistros.addClass('hidden');
    $('.is-active').removeClass('is-active');

    if (!isOpen) {
      showDistro($(this.getAttribute('href')));
    } else {
      e.preventDefault();
      location.hash = '';
      return false;
    }
  });

  $('.js-toggle-instructions').on('click', (e) => {
    var $this = $(e.target);
    var $distro = $this.closest('.js-distro-content');
    e.preventDefault();

    $('.js-install-instructions', $distro).addClass('hidden');
    $('.js-platform-' + $this.attr('data-toggle-type'), $distro).removeClass('hidden');
    scrollToDistro($distro);
  });

  window.initCopyButton('.js-copy-btn');
});
